#!/usr/bin/python
import threading
import sys
import subprocess
import time

# Thread to read data from serial and publish them
class myBridgeThread(threading.Thread):
	def __init__(self, process):
		threading.Thread.__init__(self)
		# Store process_mote as internal variable
		self.process = process

	def run(self):
		# Infinite loop to read data from mote
		while True:
			# Read new data
			sequence = self.process.stdout.readline()

			# If data is a sensor message
			if(sequence[:6] == b'Sensor'):
				# Only keep the values and replace \xff character by blank
				sequence = sequence[9:-1].replace(b'\xff',b'').decode('utf-8')
				# Split the values to single mote values
				datas = sequence[:-2].split(";")
				for data in datas:
					s = data.split(",")
					if s[1] == "1":
						# If type acceleration publish acceleration
						subprocess.call(["mosquitto_pub","-t","/node"+s[0]+"/acceleration","-m",s[2]])
					else:
						# If type temperature publish temperature
						subprocess.call(["mosquitto_pub","-t","/node"+s[0]+"/temperature","-m",s[2]])

# Read serial port from argument
port = sys.argv[1]
# Process to read data from serial
process_mote = subprocess.Popen(["make","login","TARGET=z1","MOTES="+sys.argv[1]], stdout = subprocess.PIPE, stdin = subprocess.PIPE, stderr = subprocess.PIPE)
# Process to subscribe to /gateway to know when one is joining/leaving, stdout redirected to the mote serial
process_mqtt = subprocess.Popen(["mosquitto_sub","-t","/gateway"], stdout = process_mote.stdin, stdin = subprocess.PIPE)

# Create the thread and start it
myBridge = myBridgeThread(process_mote)
myBridge.daemon = True
myBridge.start()

# Read input on the command line interface to change parameters
while True:
	x = input('Continuous sending [yes,no]: ')
	# Write to mote
	if x == "yes":
		process_mote.stdin.write("cont 1\n".encode("utf-8"))
	if x != "yes":
		process_mote.stdin.write("cont 0\n".encode("utf-8"))
