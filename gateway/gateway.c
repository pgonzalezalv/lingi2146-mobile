#include "contiki.h"

#include "sys/node-id.h"

#include "net/rime.h"

#include "dev/serial-line.h"
#include "dev/leds.h"

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include <inttypes.h>

/* Parameter dependend on the number of motes, influences the size of some variables */
#define MAXHOP 10

/* Number of connected subscribers for each type */
int sub_count_1 = 0;
int sub_count_2 = 0;

/* Structure for hello packet to build the tree */
struct packet_hello {
  uint8_t subscribed1;      /* send acceleration (default:false)? */
  uint8_t subscribed2;      /* send temperature (default:false)? */
  uint8_t sensor_cont;      /* send sensor data continuously (default:false)? */
  uint8_t next_hop;         /* hops to the root (default:MAXHOP) */
  uint8_t parents[MAXHOP];  /* parents node (default:node_id) */
};

/*---------------------------------------------------------------------------*/

PROCESS(example_broadcast_process, "Gateway");
AUTOSTART_PROCESSES(&example_broadcast_process);

/*---------------------------------------------------------------------------*/

static void
broadcast_recv(struct broadcast_conn *c, const rimeaddr_t *from)
{
}

/* Handle for sensor data from children, unicast */
static void
recv_uc(struct unicast_conn *c, const rimeaddr_t *from)
{
  char* packet = (char *) packetbuf_dataptr();
  /* Print data on serial line */
  printf("Sensor: '%s'\n", packet);

}

static const struct broadcast_callbacks broadcast_call = {broadcast_recv};
static struct broadcast_conn broadcast;

static const struct unicast_callbacks unicast_callbacks = {recv_uc};
static struct unicast_conn uc;

/*---------------------------------------------------------------------------*/

PROCESS_THREAD(example_broadcast_process, ev, data)
{
  static struct etimer timer_hello;
  static struct packet_hello pkt_hello;
  char hello[10];

  PROCESS_EXITHANDLER(broadcast_close(&broadcast);unicast_close(&uc);)

  PROCESS_BEGIN();

  broadcast_open(&broadcast, 129, &broadcast_call);
  unicast_open(&uc, 146, &unicast_callbacks);

  printf(" \n");

  /* Set hello timer to 5 seconds */
  etimer_set(&timer_hello, CLOCK_SECOND * 5);

  /* Initialize hello packet */
  pkt_hello.subscribed1 = 0;
  pkt_hello.subscribed2 = 0;
  pkt_hello.sensor_cont = 0;
  pkt_hello.next_hop = 0;
  pkt_hello.parents[0] = node_id;

  while(1) {
    PROCESS_WAIT_EVENT();

    /* Send hello messages */
    if(etimer_expired(&timer_hello)) {
      packetbuf_copyfrom(&pkt_hello, sizeof(struct packet_hello));
      broadcast_send(&broadcast);
      etimer_reset(&timer_hello);
    }

    /* Got message from serial */
    if(ev == serial_line_event_message) {
      char* s = (char *) data;

      if(strstr(s, "cont") != NULL) {
        /* Change parameter to send data continuously */
        if(s[5] == '1') {
          pkt_hello.sensor_cont = 1;
        } else {
          pkt_hello.sensor_cont = 0;
        }
      } else {
        /* Change number of subscriber listening */
        if (s[0] == '1' && s[2] == '0') {
          sub_count_1--;
        } else if (s[0] == '1' && s[2] == '1') {
          sub_count_1++;
        } else if (s[0] == '2' && s[2] == '0') {
          sub_count_2--;
        } else if (s[0] == '2' && s[2] == '1') {
          sub_count_2++;
        }

        /* If there are subscribers, activate that type of data */
        if (sub_count_1 > 0) {
          pkt_hello.subscribed1 = 1;
        } else {
          pkt_hello.subscribed1 = 0;
        }

        if (sub_count_2 > 0) {
          pkt_hello.subscribed2 = 1;
        } else {
          pkt_hello.subscribed2 = 0;
        }
      }
    }
  }

  PROCESS_END();
}
/*---------------------------------------------------------------------------*/
