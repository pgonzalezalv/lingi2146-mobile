#!/bin/sh

# Function to execute when exiting with CTRL-C
trap_handler() {
# Publish on /gateway that one is leaving
  if [ "$sensor" = "acceleration" ]; then
    mosquitto_pub -t /gateway -m "1 0"
  fi;

  if [ "$sensor" = "temperature" ]; then
    mosquitto_pub -t /gateway -m "2 0"
  fi;
  exit 1
}

# Assign trap function to CTRL-C
trap trap_handler INT

# Ask for mote number (default 2)
read -p "Node number [0,1,def=2]: " number
number=${number:-2}

# Ask for sensor type (default temperature)
read -p "Node sensor [acceleration,def=temperature]: " sensor
sensor=${sensor:-temperature}

echo listening node [$number] sensor [$sensor]
echo hit CTRL-C to quit

# Publish to /gateway that one is joining
if [ "$sensor" = "acceleration" ]; then
  mosquitto_pub -t /gateway -m "1 1"
fi;

if [ "$sensor" = "temperature" ]; then
  mosquitto_pub -t /gateway -m "2 1"
fi;

# Subscribe to sensor data
mosquitto_sub -t /node$number/$sensor
