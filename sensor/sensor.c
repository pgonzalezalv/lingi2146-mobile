#include "contiki.h"
#include "random.h"
#include "adxl345.h"

#include "sys/node-id.h"

#include "net/rime.h"

#include "dev/i2cmaster.h"  /* Include IC driver */
#include "dev/tmp102.h"     /* Include sensor driver */
#include "dev/leds.h"

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include <inttypes.h>

/* Parameter dependent on the number of motes, influences the size of some variables */
#define MAXHOP 10
/* Parameter to tell to generate random data if in cooja */
#define HARDWARE 0

/* Direct parent of the mote */
rimeaddr_t parent;
/* Timeout for direct parent */
struct ctimer ctimer_timeout;

/* Queue that contains sensor data received from childs */
char sensor_msgs[20 + MAXHOP * 20];
bool new_sensor_msg = false;

/* Precedent own sensor value */
int sensor1_old = 0;
int sensor2_old = 0;

/* Structure for hello packet to build the tree */
struct packet_hello {
  uint8_t subscribed1;      /* send acceleration (default:false)? */
  uint8_t subscribed2;      /* send temperature (default:false)? */
  uint8_t sensor_cont;      /* send sensor data continuously (default:false)? */
  uint8_t next_hop;         /* hops to the root (default:MAXHOP) */
  uint8_t parents[MAXHOP];  /* parents node (default:node_id) */
};

/* Hello packet to send */
static struct packet_hello pkt_hello;

/* Timeout handle for parent */
static void
parent_timeout(void *n)
{
  pkt_hello.next_hop = MAXHOP;
  printf("Lost\n");
  return;
}

/* Handle to output own parent and parameters */
static void verbose() {
  printf("[%d.%d]:%d:[", parent.u8[0], parent.u8[1], pkt_hello.next_hop);

  uint8_t i;
  for(i = 0; i < pkt_hello.next_hop; i++) {
    printf("%d,", pkt_hello.parents[i]);
  }

  printf("]:%d,%d;%d\n", pkt_hello.subscribed1, pkt_hello.subscribed2, pkt_hello.sensor_cont);
}

/*---------------------------------------------------------------------------*/

PROCESS(sensor_process, "Sensor");
AUTOSTART_PROCESSES(&sensor_process);

/*---------------------------------------------------------------------------*/

/* Handle to receive hello packets, broadcast */
static void
broadcast_recv(struct broadcast_conn *c, const rimeaddr_t *from)
{
  /* Read incoming packet */
  struct packet_hello *pkt_new;
  pkt_new = packetbuf_dataptr();

  /* If better nexthop */
  if(pkt_new->next_hop < pkt_hello.next_hop - 1) {
    /* Test if we are not already in the path of that message */
    uint8_t i;
    for(i = 0; i < pkt_new->next_hop; i++) {
      if(node_id == pkt_new->parents[i]) {
        return;
      }
    }

    /* Update parameters */
    pkt_hello.subscribed1 = pkt_new->subscribed1;
    pkt_hello.subscribed2 = pkt_new->subscribed2;
    pkt_hello.sensor_cont = pkt_new->sensor_cont;

    /* Update parent */
    parent.u8[0] = from->u8[0];
    parent.u8[1] = from->u8[1];

    /* Update nexthop */
    pkt_hello.next_hop = pkt_new->next_hop + 1;

    /* Update parents of the tree */
    for(i = 0; i <= pkt_new->next_hop; i++) {
      pkt_hello.parents[i] = pkt_new->parents[i];
    }
    pkt_hello.parents[pkt_hello.next_hop] = node_id;
    verbose();
    /* Set timeout */
    ctimer_set(&ctimer_timeout, CLOCK_SECOND * 12, parent_timeout, NULL);
    /* If same parent and same nexthop */
  } else if(from->u8[0] == parent.u8[0] && from->u8[1] == parent.u8[1] && pkt_new->next_hop == pkt_hello.next_hop - 1) {
    /* Reset rimout */
    ctimer_set(&ctimer_timeout, CLOCK_SECOND * 12, parent_timeout, NULL);

    /* Update parameters if they changed */
    if(pkt_hello.subscribed1 != pkt_new->subscribed1) {
      pkt_hello.subscribed1 = pkt_new->subscribed1;
      verbose();
    }
    if(pkt_hello.subscribed2 != pkt_new->subscribed2) {
      pkt_hello.subscribed2 = pkt_new->subscribed2;
      verbose();
    }
    if(pkt_hello.sensor_cont != pkt_new->sensor_cont) {
      pkt_hello.sensor_cont = pkt_new->sensor_cont;
      verbose();
    }
  }
}

/* Handle for sensor data from children, unicast */
static void
recv_uc(struct unicast_conn *c, const rimeaddr_t *from)
{
  char* packet = (char *) packetbuf_dataptr();
  /* Append message to the queue */
  strcat(sensor_msgs, packet);
  new_sensor_msg = true;
}

/*---------------------------------------------------------------------------*/

static const struct broadcast_callbacks broadcast_call = {broadcast_recv};
static struct broadcast_conn broadcast;

static const struct unicast_callbacks unicast_callbacks = {recv_uc};
static struct unicast_conn uc;

/*---------------------------------------------------------------------------*/

PROCESS_THREAD(sensor_process, ev, data)
{
  static struct etimer timer_hello, timer_sensor_retransmit, timer_new_sensor;
  char hello[10 + 3 * MAXHOP];
  char sensor_msg[20];
  int16_t raw;

  PROCESS_EXITHANDLER(broadcast_close(&broadcast); unicast_close(&uc);)

  PROCESS_BEGIN();

  broadcast_open(&broadcast, 129, &broadcast_call);
  unicast_open(&uc, 146, &unicast_callbacks);

  printf(" \n");

  /* Set hello timer to 5 seconds */
  etimer_set(&timer_hello, CLOCK_SECOND * 5);
  /* Set data retransmit timer to 1 second */
  etimer_set(&timer_sensor_retransmit, CLOCK_SECOND * 1);
  /* Set read sensor timer to 11 seconds */
  etimer_set(&timer_new_sensor, CLOCK_SECOND * 11);

  if(HARDWARE) {
    tmp102_init();
  }

  /* Initialize hello packet */
  pkt_hello.subscribed1 = 0;
  pkt_hello.subscribed2 = 0;
  pkt_hello.next_hop = MAXHOP;
  pkt_hello.parents[0] = node_id;

  while(1) {
    PROCESS_WAIT_EVENT();

    /* Send Hello message */
    if(etimer_expired(&timer_hello)) {
      /* Only if connected to a parent */
      if(pkt_hello.next_hop < MAXHOP) {
        packetbuf_copyfrom(&pkt_hello, sizeof(struct packet_hello));
        broadcast_send(&broadcast);
      }
      etimer_reset(&timer_hello);
    }

    /* Retransmit sensor data */
    if(etimer_expired(&timer_sensor_retransmit)) {
      /* Only if connected to a parent and data in queue to send */
      if(pkt_hello.next_hop < MAXHOP && new_sensor_msg) {
        new_sensor_msg = false;
        rimeaddr_t addr;
        packetbuf_copyfrom(sensor_msgs, strlen(sensor_msgs)+1);
        addr.u8[0] = parent.u8[0];
        addr.u8[1] = parent.u8[1];
        unicast_send(&uc, &addr);
        /* Empty the queue */
        strcpy(sensor_msgs, "");
      }
      etimer_reset(&timer_sensor_retransmit);
    }

    /* Read sensor */
    /* <id>,<sensor_type>,<sensor_val> */
    if(etimer_expired(&timer_new_sensor)) {
      /* Accelerometer */
      if(pkt_hello.subscribed1 && pkt_hello.next_hop < MAXHOP) {
        int sensor1;
        /* Read sensor or generate random */
        if(HARDWARE) {
          sensor1  = (int) accm_read_axis(X_AXIS);
        } else {
          sensor1 = random_rand();
        }
        /* Only if continuous sending or if data changed */
        if(pkt_hello.sensor_cont || (!pkt_hello.sensor_cont && sensor1 != sensor1_old)) {
          sensor1_old = sensor1;
          sprintf(sensor_msg, "%d,1,%d;", node_id,sensor1);
          /* Append message to the queue */
          strcat(sensor_msgs, sensor_msg);
          new_sensor_msg = true;
        }
      }

      /* Temperature */
      if(pkt_hello.subscribed2 && pkt_hello.next_hop < MAXHOP) {
        int sensor2;
        /* Read sensor or generate random */
        if(HARDWARE) {
          raw = tmp102_read_temp_raw();
          sensor2  = (int) ((raw >> 8) + 128);
        } else {
          sensor2 = random_rand();
        }
        /* Only if continuous sending or if data changed */
        if(pkt_hello.sensor_cont || (!pkt_hello.sensor_cont && sensor2 != sensor2_old)) {
          sensor2_old = sensor2;
          sprintf(sensor_msg, "%d,2,%d;", node_id,sensor2);
          /* Append message to the queue */
          strcat(sensor_msgs, sensor_msg);
          new_sensor_msg = true;
        }
      }
      etimer_reset(&timer_new_sensor);
    }
  }
  PROCESS_END();
}
/*---------------------------------------------------------------------------*/
